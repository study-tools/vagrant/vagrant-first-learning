# Install VAGRANT:
- https://www.vagrantup.com/downloads

```shell
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vagrant
```

# Documentações Oficiais:
- https://www.vagrantup.com/docs

# Download Boxes:
- https://app.vagrantup.com/boxes/search

# Criação de Uma Maquina:
1. Acessas a pagina de boxes no site ificial. 
2. Criar uma repositório específico para esta maquina.
3. Executar o comando 'vagrant init ${nomedaimagem}'
4. Iniciar a maquina com 'vagrant up'
    1.  Deve sempre existir um provider:
        - VirtualBox
        - VMware
        - Hyper-V
        - Docker
5. O acesso pode ser feito via interface do Virtualizador.
    1. user: vagrant
    2. pass: vagrant
6. O acesso porde ser feito via linha de comando com 'vagrant ssh'


